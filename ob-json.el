;;; ob-json.el --- org-babel functions for template evaluation

;; Copyright (C) Ross Donaldson

;; Author: Ross Donaldson
;; Keywords: literate programming, reproducible research
;; Homepage: https://orgmode.org
;; Version: 0.01

;;; License:

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation; either version 3, or (at your option)
;; any later version.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with GNU Emacs; see the file COPYING.  If not, write to the
;; Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
;; Boston, MA 02110-1301, USA.

;;; Commentary:

;;; Code:
(require 'ob)
(require 'ob-ref)
(require 'ob-eval)

;; optionally define a file extension for this language
(add-to-list 'org-babel-tangle-lang-exts '("json" . ".json"))

;; optionally declare default header arguments for this language
(defvar org-babel-default-header-args:json '())

;; This function expands the body of a source code block by doing
;; things like prepending argument definitions to the body, it should
;; be called by the `org-babel-execute:json' function below.
(defun org-babel-expand-body:json (body params &optional processed-params)
  "Expand BODY according to PARAMS, return the expanded body."
  (let ((vars (-filter (lambda (p) (not (symbolp p)))
                       (nth 1 (or processed-params (org-babel-process-params params))))))
    (concat
     (mapconcat ;; define any variables
      (lambda (pair)
        (format "%s=%S"
                (car pair) (org-babel-json-var-to-json (cdr pair))))
      vars "\n") "\n" body "\n")))



;; This is the main function which is called to evaluate a code
;; block.
;;
;; This function will evaluate the body of the source code and
;; return the results as emacs-lisp depending on the value of the
;; :results header argument
;; - output means that the output to STDOUT will be captured and
;;   returned
;; - value means that the value of the last statement in the
;;   source code block will be returned
;;
;; The most common first step in this function is the expansion of the
;; PARAMS argument using `org-babel-process-params'.
;;
;; Please feel free to not implement options which aren't appropriate
;; for your language (e.g. not all languages support interactive
;; "session" evaluation).  Also you are free to define any new header
;; arguments which you feel may be useful -- all header arguments
;; specified by the user will be available in the PARAMS variable.
(defun org-babel-execute:json (body params)
  "Execute a block of Json code with org-babel.
This function is called by `org-babel-execute-src-block'"
  (message "executing Json source code block")
  (let* ((processed-params (org-babel-process-params params))
         ;; variables assigned for use in the block
         (vars (second processed-params))
         (result-params (third processed-params))
         ;; either OUTPUT or VALUE which should behave as described above
         (result-type (fourth processed-params))
         ;; expand the body with `org-babel-expand-body:json'
         (full-body (org-babel-expand-body:json
                     body params processed-params)))
    ;; TODO: Add option to format the json
    full-body
    ))

(defun org-babel-json-var-to-json (var)
  "Convert an elisp var into a string of json source code
specifying a var of the same value."
  (format "%S" var))

(defun org-babel-json-table-or-string (results)
  "If the results look like a table, then convert them into an
Emacs-lisp table, otherwise return the results as a string."
  )


(provide 'ob-json)
;;; ob-json.el ends here

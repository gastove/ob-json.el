# ob-json.el

This adds support for evaluating JSON blocks in Org.
You might think, "but that's a very silly thing to do,"
and you'd be right! JSON is, after all, a data specification format;
what could evaluating it possibly mean?

My use case for writing this was: literate programming against APIs which take JSON.
It became incredibly useful to be able to perform org mode tricks like this:

``` org
Let's see if we can send data correctly to a Unix Domain Socket!

#+name: first-json
#+begin_src json
{
    "foo": 5
}
#+end_src

#+name: second-json
#+begin_src json
{
    "foo": 6
}
#+end_src

#+name some-api-function
#+begin_src shell :var data=""
echo -n $data | ncat -U $HOME/.app/my.socket
#+end_src

#+call: some-api-function(data=first-json)
#+call: some-api-function(data=second-json)
```

Without JSON support for Org Babel, this just doesn't work!
With JSON support, it's grand!
You get all the lovely support of Emacs and Org -- jumping in to a JSON buffer,
tangling, reusable source code blocks taking arguments.
It's a great way to live!

This project is... well, it has no features.
It doesn't support variable substitution (yet), nor does it respect any header args.
Eventually, I might add support for a `:pretty` arg to control formatting?
Who knows.
For now, I am happy.
